CLI (command-line interface) for substrate-based blockchains.  
based on @polkadot/api-cli (https://github.com/polkadot-js/tools/tree/master/packages/api-cli).  
packing to native bundles with nodejs pkg tool (https://github.com/vercel/pkg). 


prerequisites:  
1) nodejs version >=14  
2) pkg: npm install -g pkg  
  
how-to build:  
npm install  
pkg .  
